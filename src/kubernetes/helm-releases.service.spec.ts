import { Test, TestingModule } from '@nestjs/testing'
import { HelmReleasesService } from './helm-releases.service'

describe('HelmReleasesService', () => {
  let service: HelmReleasesService

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [HelmReleasesService],
    }).compile()

    service = module.get<HelmReleasesService>(HelmReleasesService)
  })

  it('should be defined', () => {
    expect(service).toBeDefined()
  })
})
