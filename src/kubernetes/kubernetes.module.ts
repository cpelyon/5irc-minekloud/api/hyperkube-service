import { Module } from '@nestjs/common'
import { DeploymentsService } from './deployments.service'
import { ConfigService } from './config.service'
import { PersistentVolumeClaimsService } from './persistent-volume-claims.service'
import { ServicesService } from './services.service'
import { HelmReleasesService } from './helm-releases.service'
import { PodsService } from './pods.service';
import { ExecService } from './exec.service';

@Module({
  providers: [
    DeploymentsService,
    ConfigService,
    PersistentVolumeClaimsService,
    ServicesService,
    HelmReleasesService,
    PodsService,
    ExecService,
  ],
  exports: [
    DeploymentsService,
    PersistentVolumeClaimsService,
    ServicesService,
    HelmReleasesService,
    ExecService,
  ],
})
export class KubernetesModule {}
