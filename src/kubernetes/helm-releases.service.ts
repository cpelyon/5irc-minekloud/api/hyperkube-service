import { CustomObjectsApi } from '@kubernetes/client-node'
import { Injectable } from '@nestjs/common'
import { ConfigService } from './config.service'
import { readFile } from 'fs/promises'
import { safeLoad } from 'js-yaml'
import { ServerConfig } from 'src/servers/server.config'
import { CRDHelmRelease } from './models/CRDHelmRelease'

@Injectable()
export class HelmReleasesService {
  client: CustomObjectsApi

  constructor(configService: ConfigService) {
    this.client = configService.getCustomObjectClient()
  }

  async deleteServerHelmRelease(namespace: string, serverId: string) {
    try {
      const res = (await this.client.deleteNamespacedCustomObject(
        'helm.toolkit.fluxcd.io',
        'v2beta1',
        namespace,
        'helmreleases',
        `server-${serverId}`,
      )) as any
      console.info(res.code)
    } catch (error) {
      console.log('Error deleteServerHelmRelease')
      console.log(error)
    }
  }

  async getHelmReleases(namespace: string): Promise<CRDHelmRelease[]> {
    try {
      const existingCRD = (await this.client.listNamespacedCustomObject(
        'helm.toolkit.fluxcd.io',
        'v2beta1',
        namespace,
        'helmreleases',
        undefined,
        undefined,
        undefined,
        'managedby=hyperkube'
      )) as any
      return existingCRD.body.items as CRDHelmRelease[]
    } catch (error) {
      console.log('Error getHelmReleases')
      console.log(error)
      return []
    }
  }

  async deployServerHelmRelease(namespace: string, serverId: string, serverConfig: ServerConfig) {
    const file = await readFile('templates/helm-release.template.yml', 'utf8')
    const specs = safeLoad(file) as CRDHelmRelease

    specs.metadata.name = `server-${serverId}`
    specs.metadata.namespace = namespace
    specs.metadata.labels['serverId'] = serverId
    specs.spec.values = serverConfig

    const labelSelector = `app=minecraft-server,serverId=${serverId}`
    await this.apply(namespace, labelSelector, specs)
  }

  async apply(
    namespace: string,
    labelSelector: string,
    specs: any,
  ): Promise<CRDHelmRelease | null> {
    let existingCRD = null
    try {
      existingCRD = (await this.client.listNamespacedCustomObject(
        'helm.toolkit.fluxcd.io',
        'v2beta1',
        namespace,
        'helmreleases',
        undefined,
        undefined,
        undefined,
        labelSelector,
      )) as any
    } catch (error) {
      console.log('Error apply-listNamespacedCustomObject')
      console.log(error)
      return null
    }
    
    if (existingCRD){
      const crdHelmRelease = existingCRD.body.items as CRDHelmRelease[]
      try {
        // If CRD already exist we use replace
        if (crdHelmRelease.length > 0) {
          const res = await this.client.patchNamespacedCustomObject(
            'helm.toolkit.fluxcd.io',
            'v2beta1',
            namespace,
            'helmreleases',
            specs.metadata.name,
            specs,
            undefined,
            undefined,
            undefined,
            { headers: { 'content-type': 'application/merge-patch+json' } },
          )
          return res.body as CRDHelmRelease
        }
      } catch (error) {
        console.log('Error apply-patchNamespacedCustomObject')
        console.log(error)
      }
    }
   
    // Create CRD
    try {
      const res = await this.client.createNamespacedCustomObject(
        'helm.toolkit.fluxcd.io',
        'v2beta1',
        namespace,
        'helmreleases',
        specs,
      )
      return res.body as CRDHelmRelease
    } catch (error) {
      console.log('Error apply-createNamespacedCustomObject')
      console.log(error)
    }
  }
}
