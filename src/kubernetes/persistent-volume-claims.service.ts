import { CoreV1Api, V1PersistentVolumeClaim } from '@kubernetes/client-node'
import { Injectable } from '@nestjs/common'
import { readFile } from 'fs/promises'
import { ConfigService } from './config.service'
import { safeLoad } from 'js-yaml'

@Injectable()
export class PersistentVolumeClaimsService {
  client: CoreV1Api

  constructor(configService: ConfigService) {
    this.client = configService.getCoreClient()
  }

  async deleteServerPvc(namespace, serverId) {
    await this.client.deleteNamespacedPersistentVolumeClaim(
      `server-${serverId}-claim`,
      namespace,
    )
  }

  async deployServerPvc(namespace, serverId, capacity) {
    const file = await readFile(
      'templates/persistent-volume-claim.template.yml',
      'utf8',
    )
    const specs = safeLoad(file) as V1PersistentVolumeClaim

    specs.metadata.name = `server-${serverId}-claim`
    specs.spec.resources.requests['storage'] = capacity

    const fieldSelector = `metadata.name=server-${serverId}-claim`

    await this.apply(namespace, fieldSelector, specs)
  }

  async apply(
    namespace: string,
    fieldSelector: string,
    specs: V1PersistentVolumeClaim,
  ): Promise<V1PersistentVolumeClaim> {
    const existingPvc = await this.client.listNamespacedPersistentVolumeClaim(
      namespace,
      undefined,
      undefined,
      undefined,
      fieldSelector,
    )

    // If Deployment already exist we use replace
    if (existingPvc.body.items.length > 0) {
      // PVC are immutable exept storage capacity but need to use a patch
      return existingPvc.body.items[0]

      const res = await this.client.replaceNamespacedPersistentVolumeClaim(
        specs.metadata.name,
        namespace,
        specs,
      )
      return res.body
    }

    // Create Deployment
    const res = await this.client.createNamespacedPersistentVolumeClaim(
      namespace,
      specs,
    )
    return res.body
  }
}
