import { AppsV1Api, V1Deployment } from '@kubernetes/client-node'
import { Injectable } from '@nestjs/common'
import { readFile } from 'fs/promises'
import { ConfigService } from './config.service'
import { safeLoad } from 'js-yaml'

@Injectable()
export class DeploymentsService {
  client: AppsV1Api

  constructor(configService: ConfigService) {
    this.client = configService.getAppsClient()
  }

  async deleteServerDeployment(namespace: string, serverId: string) {
    await this.client.deleteNamespacedDeployment(
      `server-${serverId}-deployment`,
      namespace,
    )
  }

  async deployServerDeployment(namespace: string, serverId: string) {
    const file = await readFile(
      'templates/minecraft-server-deployment.template.yml',
      'utf8',
    )
    const specs = safeLoad(file) as V1Deployment

    specs.metadata.name = `server-${serverId}-deployment`
    specs.metadata.labels['serverId'] = serverId
    specs.spec.selector.matchLabels['serverId'] = serverId
    specs.spec.template.metadata.labels['serverId'] = serverId
    specs.spec.template.spec.volumes[0].persistentVolumeClaim.claimName = `server-${serverId}-claim`

    const labelSelector = `app=minecraft-server,serverId=${serverId}`

    await this.apply(namespace, labelSelector, specs)
  }

  async apply(
    namespace: string,
    labelSelector: string,
    specs: V1Deployment,
  ): Promise<V1Deployment> {
    const existingDeployment = await this.client.listNamespacedDeployment(
      namespace,
      undefined,
      undefined,
      undefined,
      undefined,
      labelSelector,
    )

    // If Deployment already exist we use replace
    if (existingDeployment.body.items.length > 0) {
      const res = await this.client.replaceNamespacedDeployment(
        specs.metadata.name,
        namespace,
        specs,
      )
      return res.body
    }

    // Create Deployment
    const res = await this.client.createNamespacedDeployment(namespace, specs)
    return res.body
  }
}
