import { CrossNamespaceObjectReference } from './CrossNamespaceObjectReference'

export interface HelmChartTemplateSpec {
  chart: string
  version?: string 
  sourceRef: CrossNamespaceObjectReference
  interval: string
}