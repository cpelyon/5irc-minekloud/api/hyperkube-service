export interface CrossNamespaceObjectReference {
  apiVersion?: string
  kind: string
  name: string
  namespace?: string
}