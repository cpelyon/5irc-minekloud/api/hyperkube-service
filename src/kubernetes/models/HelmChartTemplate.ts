import { HelmChartTemplateSpec } from './HelmChartTemplateSpec'

export interface HelmChartTemplate {
  spec: HelmChartTemplateSpec
}