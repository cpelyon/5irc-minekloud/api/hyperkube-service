import { HelmChartTemplate } from './HelmChartTemplate'
import { ServerConfig } from '../../servers/server.config'

export interface HelmReleaseSpec {
  chart:HelmChartTemplate
  interval:string

  values?: ServerConfig
}