import { V1ObjectMeta, V1CustomResourceDefinitionStatus } from '@kubernetes/client-node'
import { HelmReleaseSpec } from './HelmReleaseSpec'

export interface CRDHelmRelease {
    'apiVersion'?: string
    'kind'?: string
    'metadata'?: V1ObjectMeta
    'spec': HelmReleaseSpec
    'status'?: V1CustomResourceDefinitionStatus
}
