import { CoreV1Api, V1Service } from '@kubernetes/client-node'
import { Injectable } from '@nestjs/common'
import { readFile } from 'fs/promises'
import { ConfigService } from './config.service'
import { safeLoad } from 'js-yaml'

@Injectable()
export class ServicesService {
  client: CoreV1Api

  constructor(configService: ConfigService) {
    this.client = configService.getCoreClient()
  }

  async deleteServerService(namespace: string, serverId: string) {
    await this.client.deleteNamespacedService(
      `server-${serverId}-deployment`,
      namespace,
    )
  }

  async deployServerService(namespace: string, serverId: string) {
    const file = await readFile(
      'templates/minecraft-server-service.template.yml',
      'utf8',
    )
    const specs = safeLoad(file) as V1Service

    specs.metadata.name = `server-${serverId}-service`
    specs.spec.selector['serverId'] = serverId

    const labelSelector = `metadata.name=${specs.metadata.name}`

    await this.apply(namespace, labelSelector, specs)
  }

  async apply(
    namespace: string,
    fieldSelector: string,
    specs: V1Service,
  ): Promise<V1Service> {
    const existingService = await this.client.listNamespacedService(
      namespace,
      undefined,
      undefined,
      undefined,
      fieldSelector,
    )

    // If Deployment already exist we use replace
    if (existingService.body.items.length > 0) {
      // Services are immutable replace doesn't work, need to use a patch
      return existingService.body.items[0]
      const res = await this.client.replaceNamespacedService(
        specs.metadata.name,
        namespace,
        specs,
      )
      return res.body
    }

    // Create Deployment
    const res = await this.client.createNamespacedService(namespace, specs)
    return res.body
  }
}
