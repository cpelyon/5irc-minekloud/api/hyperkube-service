import {
  AppsV1Api,
  CoreV1Api,
  CustomObjectsApi,
  KubeConfig,
} from '@kubernetes/client-node'
import { Injectable } from '@nestjs/common'

@Injectable()
export class ConfigService {
  private kubeConfig: KubeConfig = new KubeConfig()

  constructor() {
    this.kubeConfig.loadFromDefault()
    if (process.env.LOAD_FROM_CLUSTER == 'true') {
      this.kubeConfig.loadFromCluster()
    }

  }

  getKubeConfig(): KubeConfig {
    return this.kubeConfig;
  }

  getAppsClient() {
    return this.kubeConfig.makeApiClient(AppsV1Api)
  }

  getCoreClient() {
    return this.kubeConfig.makeApiClient(CoreV1Api)
  }

  getCustomObjectClient() {
    return this.kubeConfig.makeApiClient(CustomObjectsApi)
  }
}
