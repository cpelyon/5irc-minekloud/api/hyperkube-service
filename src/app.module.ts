import { Module } from '@nestjs/common'
import { ScheduleModule } from '@nestjs/schedule'
import { ConfigModule } from '@nestjs/config'
import { KubernetesModule } from './kubernetes/kubernetes.module'
import { ServersModule } from './servers/servers.module'

@Module({
  imports: [
    KubernetesModule,
    ServersModule,
    ScheduleModule.forRoot(),
    ConfigModule.forRoot({ isGlobal: true, envFilePath: 'config.env' }),
  ],
  controllers: [],
  providers: [],
})
export class AppModule {}
