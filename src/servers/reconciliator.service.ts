import { HttpService, Injectable } from '@nestjs/common'
import { Interval } from '@nestjs/schedule'
import { ConfigService } from '@nestjs/config'

import { ServerDto } from './dto/server.dto'
import { ServerStatus } from './models/serverStatus'
import { ServersService } from './servers.service'
import { plainToClass } from 'class-transformer'

@Injectable()
export class Reconciliator {
  private serversServiceBaseUrl: string
  //private reconcileInterval: number

  constructor(
    private serversService: ServersService,
    private httpService: HttpService,
    private configService: ConfigService,
  ) {
    this.serversServiceBaseUrl = this.configService.get<string>(
      'SERVERS_SERVICE_ADDR',
    )
    //this.reconcileInterval = this.configService.get<number>('RECONCILE_INTERVAL')
  }

  @Interval(Number(process.env.RECONCILE_INTERVAL) || 10000)
  async reconcile() {
    // Pull servers from servers-service
    const pulledServers = await this.pullServers()
    if (!pulledServers) {
      return
    }

    // Foreach serveurs not deleted, create or replace serveur
    await Promise.all(
      pulledServers
        .filter(server => server.deletedAt == null)
        .map(pulledServer => this.createOrReplaceServer(pulledServer))
    )

    if (!this.configService.get<boolean>('RECONCILE_DELETE')) {
      return
    }
    // Delete unused HelmReleases
    const pulledServerIdsDeleted = pulledServers
      .filter(pulledServer => pulledServer.deletedAt != null)
      .map(server => server.id)

    const servers = await this.serversService.getServers()
    await Promise.all(
      servers.map((server) => {
        const serverId = server.metadata.labels.serverId as string
        if (pulledServerIdsDeleted.includes(serverId)) {
          console.log(`Reconcile: delete server with id ${serverId}`)
          return this.serversService.deleteServer(serverId)
        }
      }),
    )
  }

  async reconcileByServerId(serverId: string) {
    const pulledServer = await this.pullServer(Number(serverId))
    if (!pulledServer) {
      return
    }

    if ( pulledServer.deletedAt != null && this.configService.get<boolean>('RECONCILE_DELETE')) {
      console.log(`Reconcile: delete server with id ${serverId}`)
      await this.serversService.deleteServer(serverId)
      return
    }

    if (pulledServer.deletedAt == null) {
      await this.createOrReplaceServer(pulledServer)
    }
  }

  async createOrReplaceServer({id, status, maxAvailableRAM}: ServerDto): Promise<void>{
    console.info(`Reconcile: create/update server with id ${id}`)

    // Minimum 1G
    const ramLimit = Math.max(maxAvailableRAM, 1)

    return this.serversService.createOrReplaceServer(id, {
      online: status == ServerStatus.ON,
      serverId: id.toString(),
      resources: {
        limits: {
          memory: `${ramLimit}G`
        }
      }
    })
  }

  async pullServer(serverId: number): Promise<ServerDto | null> {
    const result = await this.httpService
      .get(`${this.serversServiceBaseUrl}/${serverId}`)
      .toPromise()
      .then(res => {
        return plainToClass(ServerDto, res.data)
      })
      .catch(() => {
        return null
      })
    return result
  }

  async pullServers(): Promise<ServerDto[] | null> {
    const result = await this.httpService
      .get(this.serversServiceBaseUrl + '?withDelete=true')
      .toPromise()
      .then(res => {
        return plainToClass(ServerDto, res.data)
      })
      .catch(() => {
        return null
      })
    return result
  }
}
