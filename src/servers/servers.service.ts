import { Injectable } from '@nestjs/common'
import { HelmReleasesService } from 'src/kubernetes/helm-releases.service'
import { CRDHelmRelease } from 'src/kubernetes/models/CRDHelmRelease'
import { ServerConfig } from './server.config'

@Injectable()
export class ServersService {
  constructor(private helmReleaseService: HelmReleasesService) {}

  async createOrReplaceServer(serverId: string, serverConfig: ServerConfig) {
    const namespace = 'default'
    await this.helmReleaseService.deployServerHelmRelease(namespace, serverId, serverConfig)
  }

  async getServers(): Promise<CRDHelmRelease[]> {
    const namespace = 'default'
    return await this.helmReleaseService.getHelmReleases(namespace)
  }

  async deleteServer(serverId: string) {
    const namespace = 'default'
    await this.helmReleaseService.deleteServerHelmRelease(namespace, serverId)
    return true
  }
}
