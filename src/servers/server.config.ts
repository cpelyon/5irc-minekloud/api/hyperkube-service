export interface ServerConfig {
  online: boolean
  serverId: string
  resources?: {
    limits?: {
      cpu?: string,
      memory?: string,
    },
    requests?: {
      cpu?: string,
      memory?: string,
    },
  }
}
