import { Controller, HttpStatus, Param, Post, HttpCode, Body, InternalServerErrorException, ServiceUnavailableException } from '@nestjs/common'
import { ExecService } from 'src/kubernetes/exec.service'
import { CommandDto } from './dto/command.dto'
import { Reconciliator } from './reconciliator.service'

@Controller()
export class ServersController {
  constructor(private reconcilatorService: Reconciliator, private execService: ExecService) { }

  @Post('notifyserverchange/:serverId')
  @HttpCode(HttpStatus.CONTINUE)
  async updateServer(@Param('serverId') serverId: string) {
    try {
      await this.reconcilatorService.reconcileByServerId(serverId)
    } catch (error) {
      console.log(error)
    }
  }

  @Post('sendcommand/:serverId')
  @HttpCode(200)
  async sendCommand(@Param('serverId') serverId: string, @Body() commandPayload: CommandDto){
    const result = await this.execService.execCommandOnServer('default', serverId, commandPayload.command);
    if(!result) {
      throw new ServiceUnavailableException()
    }

    return;
  }
}
