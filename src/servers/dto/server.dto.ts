import { Type } from 'class-transformer'
import { IsString, IsNumber, IsEnum, IsDate, IsNotEmpty } from 'class-validator'
import { ServerStatus } from '../models/serverStatus'
export class ServerDto {
  @IsString()
  @IsNotEmpty()
  @Type(() => String)
  id: string
  @IsNumber()
  idUser : number
  @IsString()
  name: string
  @IsString()
  hostname: string
  @IsEnum(ServerStatus)
  @IsNotEmpty()
  status: ServerStatus
  @IsNumber()
  maxAvailableRAM: number
  @IsDate()
  deletedAt: Date
}