import { IsString } from 'class-validator'
export class CommandDto {
  @IsString()
  command: string
}
